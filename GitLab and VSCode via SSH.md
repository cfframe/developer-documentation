# GitLab and VS Code via SSH
A primary focus here is to enable secure communications between a local git repo and its online repository whilst minimising on-going prompts to enter a password or passphrase.  Much of what is written here will apply to other repositories such as GitHub.

## Assumptions
- You have Administrator permissions on your laptop
- Visual Studio Code (VS Code) installed
- Git installed using default locations
- Working with Windows 10 or 11
- GitLab account created
- Some knowledge of Git commands
- Using PowerShell for terminal commands

## General
There are a few ways of setting up security, some deemed more secure than others.  SSH in particular is considered more secure than HTTPS, but it is easy to set up in a way that causes much inconvenience with persistent prompts for a private key's passphrase.  Setting up without a passphrase is one way to simplify day-to-day usage but is clearly insecure eg if a laptop were stolen and the private key recovered.

## Create SSH key on Windows
1. Create SSH public/private key pair in PowerShell with command `ssh-keygen`, with an appropriate root file name eg 

        `ssh-keygen  -t ecdsa -b 256 -f ecdsa_gitlabme` 
    
    generates keys based on the ecdsa algorithm to 256 bits with a base name of `ecdsa_gitlabme`.  Do add a passphrase, and keep the default target folder of `C:/Users/<USERAME>/.ssh`.  

    This generates two files with the same root. For the example above, these would be:
    - ecdsa_gitlabme (private key)
    - ecdsa_gitlabme.pub (public key)


1. Create/update your global Git config file. By default, this is located at `C:\Users\<USERNAME>\.ssh\config` (with no extension). Add the following content, with `<USERNAME>` and `ecdsa_gitlabme` updated as appropriate:

        # GitLab.com
        Host gitlab.com
            Preferredauthentications publickey
            IdentityFile C:/Users/<USERNAME>/.ssh/ecdsa_gitlabme
            AddKeysToAgent yes
    
    Note that other repositories can also be added here eg for GitHub, ideally with a separate key.

## Add SSH key to GitLab
1. On your local machine, navigate to `C:\Users\<USERNAME>\.ssh\`. Open the public key (eg ecdsa_gitlabme.pub) in a text editor and copy its entire content.
1. In GitLab, navigate to User Settings (aka Preference) eg via the profile icon in the top right corner.
1. Select `SSH Keys`
1. In the "Key" box under, paste the content of your public key. This will automatically populate a number of other fields on that page.
1. Optionally set an expiry date.
1. Click `Add key`.

## Configure the ssh-agent

1. Open Windows Services as Administrator 

    eg in task bar's Search bar, type `services`, right click on the Services app and choose `Run as administrator'

1. Article [How to make Powershell remember the SSH key passphrase. (github.com)](https://gist.github.com/danieldogeanu/16c61e9b80345c5837b9e5045a701c99) has some detail, but essentially do the following:
    1. Start the ssh-agent service (full name OpenSSH Authentication Agent) and configure the Startup type as `Automatic`
    1. Configure Git to use the Windows implementation of ssh instead of Git's: in PowerShell, issue this command:

        `git config --global core.sshCommand C:/Windows/System32/OpenSSH/ssh.exe`

    1. Add your key to ssh-agent via PowerShell eg: 

        `ssh-add C:/Users/cframe/.ssh/ecdsa_gitlabme`

## Create project in GitLab and clone to local machine
1. Open PowerShell and within that navigate to your local GitLab area eg `cd C:/GitLab`
1. Log into your GitLab account, create a project (eg a Blank one, with just a ReadMe.md file in it)
1. Clone the project
    1. In GitLab, reveal the "Clone" dropdown and from the "Open in your IDE" section choose "Visual Studio Code (SSH)"
1. Test it eg in VS Code, pull from the repository.
1. Restart VS Code, verify it still works.
    

    

